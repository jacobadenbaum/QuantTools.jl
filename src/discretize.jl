#=
This file contains a collection of code for discretizing a continuous
AR(p) random variable into a discrete state markov process
=#


"""
```
rouwenhorst(N::Int, ρ::Real, σ::Real, μ::Real=0.0)
```
Compute a discrete space markov approximation of an AR(1) process that
follows
```math
    y_t = \\mu + \\rho y_{t-1} + \\epsilon_t
```
using the Rouwenhorst method.  This function constructs an approximation
to the process using N equally spaced grid points.  

##### Arguments
-   `N::Int` : Number of grid points in the approximation
-   `ρ::Real`: Persistence Parameter in the AR(1) process
-   `σ::Real`: Standard deviation of the innovations in the AR(1)
-   `μ::Real`: Mean of the AR(1) (default is 0)

##### Returns
-   `grid::Linspace{Float64}` : A grid containing the approximation
    points
-   `Π::Matrix{Float64}` : A matrix whose entries are the transition
    probabilities for the process
"""
function rouwenhorst(N::Int, ρ::Real, σ::Real, μ::Real=0.0)
    
    σy  = σ/sqrt(1-ρ^2)
    p   = (1+ρ)/2
    ψ   = sqrt(N-1)*σy
    m   = μ/(1-ρ)
    
    grid= linspace(m-ψ, m+ψ, N)
    Π   = _rouwenhorst(p, p, N)
    return grid, Π
end

function _rouwenhorst(p::Real, q::Real, n::Int)
    
    if n == 2
        return [p   1-p
                1-q q]
    elseif n > 2
        O   = zeros(n-1)
        Θn1 = _rouwenhorst(p, q, n-1)
        ΘN  =   p*[Θn1   O
                   O'    0] +
            (1-p)*[O    Θn1
                   0    O'] +
            (1-q)*[O'   0
                   Θn1  O] +
                q*[0    O'
                   O    Θn1]
        ΘN[2:end-1,:] *= 1/2 
        return ΘN
    end

end
