# QuantTools

This package collects a suite of tools that I find useful in my
quantitative work.  I may spin off some of the features into another
package at a later date.  
